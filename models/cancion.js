var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cancion = new Schema(
    {
        titulo : {
            type: String,
            required:'El titulo de la cancion es requerido'
        },

        nombreArtista:{
            type:String,
            required:'Nombre del artista es requerido'
        },
        genero:{
            type:String,
            required:'Genero de la cancion es requerido'
        },
        fechaLanzamiento:{
            type:Date,
            required:'Ingrese la fecha de lanzamiento'
        },
        _id: false
    });

module.exports = mongoose.model('cancion',cancion);