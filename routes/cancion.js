var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var cancion  =  mongoose.model('cancion');

/* GET users listing. */
router.get('/', function(req, res, next) {
 cancion.find( function (err,canciones) {
     if(err){
       res.status(400).send(err);
     }
     res.json(canciones);
 });
});

router.post('/',function (req,res) {
    var nueva_cancion  = new cancion(req.body);
    nueva_cancion.save(function (err,cancion) {
      if(err){
        console.log(req)
        res.status(400).send(err);
      }
        res.json(cancion);
    });
});

router.get('/:cancionid',function (req,res) {
    cancion.findById(req.params.cancionid, function (err,cancion) {
        if (err){
          res.send(err);
        }
        res.json(cancion);
    });
});

router.delete('/:cancionid',function (req,res) {
    cancion.remove({_id: req.params.cancionid}, function (err,cancion) {
        if(err){
          res.send(err);
        }
        res.json({message:'La cancion eliminada fue: ' + req.params.cancionid});
    });
});

module.exports = router;

router.put('/:cancionid',function (req,res) {
   cancion.findOneAndUpdate({_id: req.params.cancionid},req.body,{new: true},function (err,cancion) {
       if (err){
         res.send(err);
       }
      res.json(cancion);
   })
});